class AdminController < ApplicationController
  layout 'admin'
  before_action :authenticate


  def index
    if params[:term]
      @deals = Deal.recent.where(deal_number: params[:term])
    else
      @deals = Deal.recent
    end
  end

  def destroy_deal
    @deal = Deal.find(params[:deal_id])
    @deal.destroy
    redirect_to admin_path
  end

  def show_deal
    @deal = Deal.find(params[:id])
  end
  def checkout_deal
    status = false
    @deal = Deal.find(params[:id])
    status = !@deal.checked
    @deal.update( checked: status)
    render json: { update: status}, status: :ok
  end
end
