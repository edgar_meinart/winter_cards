class DealsController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include ApplicationHelper

  before_action :authenticate, only: [:index]

  def index
  end

  def new
    current_language = I18n.locale.to_s
    redirect_to root_path if count_of_all_cards_in_bucket == 0
    @countries = $countries_of_europe['europe'].map { |value| [value[1][current_language], value[0]] }
    @deal = Deal.new
  end

  def create
    @deal = Deal.new(deal_params)
    @deal.deal_price = current_price_for_bucket
    @deal.ordered_cards = collect_cards_as_array
    @deal.mail_price = current_mail_price
    price_for_one = {}
    CARDS_TYPES.each {|card| price_for_one[card] = cards_price_for(card)}
    if @deal.save
      p = {
        cards: CARDS_TYPES.map{ |card| session[card] },
        deal: @deal,
        total_price: total_price,
        current_mail_price: current_mail_price,
        cards_prices: price_for_one
      }
      # PaymentMailer.checkout(p).deliver

      destroy_bucket
      redirect_to @deal
    else
      render action: :new
      redirect_to root_path
    end
  end

  def show
    render 'success'
  end

  private
    def deal_params
      params.require(:deal).permit :full_name, :email, :tel_number, :country, :city, :street, :index, :comment
    end
end
