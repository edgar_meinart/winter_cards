class Admin::CardsController < AdminController
  before_action :get_card, except: [:new, :index]
  def index
    @cards = Card.all.desc :created_at
  end

  def new
    @card = Card.new
  end

  def create
    @card = Card.new card_param
    if @card.save
      redirect_to admin_cards_path
    else
      render action: 'new'
    end
  end

  def update
    if @card.update_attributes card_param 
      redirect_to admin_cards_path
    else
      render action: 'edit'
    end
  end

  def destroy
    if @card.destroy
      redirect_to admin_cards_path
    end
  end

  private
  def get_card
    @card = Card.where(identifier: params[:id]).first
  end
  def card_param
    params.require(:card).permit :title, :description, :price, :image
  end
end
