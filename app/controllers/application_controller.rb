class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :content_language
  before_action :init_bucket
  CARDS_TYPES = %w(card1 card2 card3)

  helper_method :count_of_all_cards_in_bucket
  helper_method :price_for_one_card_for
  helper_method :price_per_mail_for
  helper_method :count_of_specify_card


  private
    def collect_cards_as_array
      array = []
      CARDS_TYPES.each do |card|
        array << [ card , count_of_specify_card(card) ]
      end
      return array
    end

    def init_bucket
      CARDS_TYPES.each { |card| session[card] ||= 0 }
    end

    def destroy_bucket
      CARDS_TYPES.each { |card| session[card] = 0 }
    end

    def count_of_all_cards_in_bucket
      count = 0
      CARDS_TYPES.each { |card| count += session[card] }
      count
    end

    def count_of_specify_card(card)
      session[card]
    end

    def add_to_basket_one_card(card)
      if CARDS_TYPES.include?(card)
        session[card] += 1;
      end
      session[card]
    end

    def remove_from_basket_one_card(card)
      if CARDS_TYPES.include?(card) && session[card] > 0
        session[card] -= 1;
      end
      session[card]
    end

    def price_for_one_card_for(count_of_cards)
      value_ls = 0.8
      value_eur = 1.42287
      if count_of_cards >= 3 && count_of_cards < 9
        value_ls = 0.7
        value_eur = 0.9966
      elsif count_of_cards >= 9
        value_ls = 0.6833
        value_eur = 0.9722
      elsif count_of_cards < 3
        value_ls = 0.8
        value_eur = 1.42287
      end
      {lv: value_ls, eur: value_eur * 1.47}
    end

    def price_per_mail_for(cards_count)
      value = 0;
      if cards_count <= 5
        value = 40
      elsif cards_count <= 10
        value = 50
      elsif cards_count <= 20
        value = 60
      elsif cards_count <= 30
        value = 70
      elsif cards_count > 30
        value = 100
      end
      {lv: value, eur: value }
    end

    def authenticate
      authenticate_or_request_with_http_basic do |username, password|
        username == "admin" && password == "password"
      end
    end

    def content_language
      session[:current_language] ||= 'lv'
      @current_language = session[:current_language]
      I18n.locale = session[:current_language]
    end

end
