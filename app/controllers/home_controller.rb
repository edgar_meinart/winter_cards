class HomeController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include ApplicationHelper

  def index
  end

  def add_to_basket
    count = add_to_basket_one_card params[:card_id]
    total_cards_price_text = current_price_for_bucket
    total_mail_price_text = current_mail_price
    render json: { count: count, total_price_text: total_cards_price_text , mail_price_text: total_mail_price_text}, status: :ok
  end

  def remove_from_basket
    count = remove_from_basket_one_card params[:card_id]
    total_cards_price_text = current_price_for_bucket
    total_mail_price_text = current_mail_price
    render json: { count: count, total_price_text: total_cards_price_text , mail_price_text: total_mail_price_text}, status: :ok
  end


  def set_language
    if params[:language] == 'rus' || params[:language] == 'lv'
      session[:current_language] = params[:language]
    end
    render nothing: true, status: 200
  end


  private

    def cart_params
      params.require(:cart).permit :count, :card_id
    end
end
