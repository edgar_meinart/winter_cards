class PaymentMailer < ActionMailer::Base
  helper ApplicationHelper
  default from: "payment@cards.ivi.lv"

  def checkout(params)
    @params = params #dimonstrikm@gmail.com
    mail(to: "#{@params[:deal].email}; dimonstrikm@gmail.com", subject: "cards.ivi.lv  Payment information for #{@params[:deal].deal_number } ")
  end

end
