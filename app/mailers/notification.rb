class Notification < ActionMailer::Base
  default from: "notification@wimb.com"

  def login_notification(params)
    mail to: "edgars@meinarts.name", subject: "Login from #{params[:ip]} at #{params[:time]}" do |format|
      format.text do
        text = ""
        text << params[:message] + ' '
        text << params[:ip] + ' '
        text << params[:client].env['HTTP_USER_AGENT'].to_s + ' '
        text << params[:time].to_s + ' '
        render text: text
      end
    end
  end

  def checkout(params)

  end
end
