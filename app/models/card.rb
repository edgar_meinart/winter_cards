class Card
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip

  has_mongoid_attached_file :image,format: :png
  
  has_and_belongs_to_many :deals

  field :title, type: String
  field :description, type: String
  field :price, type: Integer
  field :identifier, type: String
  index identifier: 1

  belongs_to :deal

  before_save :generate_code


  def to_param
  	self.identifier.to_s
  end

  public
    def generate_code
      if self.identifier.blank?
        self.identifier = SecureRandom.hex(2)
        generate_code if Card.where(identifier: self.identifier).present?
      end
    end

    def self.cards_in_bucket(session)
      cards = Card.all
      bucket = []
      cards.each do |card|
        bucket << {id: card.id.to_s, image_url: card.image.url.to_s, count: session[card.id], card: card} if !session[card.id].nil?
      end
      bucket
    end

end
