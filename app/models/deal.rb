class Deal
  include Mongoid::Document
  include Mongoid::Timestamps

  has_and_belongs_to_many :cards

  field :deal_price, type: String, default: 0
  field :mail_price, type: String, default: 0

  field :ordered_cards, type: Array, default: []

  field :full_name, type: String
  field :email, type: String
  field :tel_number, type: String

  field :country, type: String
  field :city, type: String
  field :street, type: String
  field :index, type: String

  field :deal_number, type: String, default: ''

  field :checked, type: Boolean, default: false

  field :count_of_cards, type: Integer, default: 0

  field :comment, type: String

  field :verify, type: Boolean, default: false

  validates_presence_of :full_name, :email
  validates_presence_of :country, :city, :street, :index

  before_create :generate_code

  has_many :cards

  scope :recent, -> { order_by(:created_at => :desc) }

  private

    def generate_code
      if self.deal_number.blank?
        # self.deal_number = SecureRandom.hex(2)
        # generate_code if Deal.with(deal_number: self.deal_number).present?
      end
    end




end
