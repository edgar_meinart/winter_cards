class Settings
  include Mongoid::Document
  field :cards_ids, type: Array, default: []
end
