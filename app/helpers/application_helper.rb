module ApplicationHelper
  def current_price_for_bucket
    cards_count = count_of_all_cards_in_bucket
    price_for_one = price_for_one_card_for cards_count
    "#{number_to_currency( cards_count * price_for_one[:lv], unit: 'Ls', precision: 2)} / #{number_to_currency( cards_count * price_for_one[:lv] * 1.42, unit: '€', precision: 2)}"
  end
  def current_mail_price
    cards_count = count_of_all_cards_in_bucket
    price_for_mail = price_per_mail_for cards_count
    "#{number_to_currency( price_for_mail[:lv] / 100.0, unit: 'Ls', precision: 2)} / #{number_to_currency( (price_for_mail[:eur] / 100.0) * 1.42, unit: '€', precision: 2)}"
  end
  def total_price
    cards_count = count_of_all_cards_in_bucket
    price_for_one = price_for_one_card_for cards_count
    price_for_mail = price_per_mail_for cards_count
    total = {
      lv: cards_count * price_for_one[:lv] + (price_for_mail[:lv] / 100.0),
      eur: cards_count * price_for_one[:eur] + (price_for_mail[:eur] / 100.0)
    }
  end
  def cards_price_for(card)
    cards_count = count_of_specify_card card
    price_for_one = price_for_one_card_for count_of_all_cards_in_bucket
    "#{number_to_currency( cards_count * price_for_one[:lv], unit: 'Ls', precision: 2)} / #{number_to_currency( cards_count * price_for_one[:lv] * 1.42, unit: '€', precision: 2)}"
  end
end
