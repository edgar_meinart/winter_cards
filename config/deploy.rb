# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'winter_cards'
set :repo_url, 'git@bitbucket.org:edgar_meinart/winter_cards.git'
# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, "/home/web/apps/winter_cards"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join("#{fetch(:deploy_to)}/shared/restart.txt")
      # p '==========================================='
      # execute :ls
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # execute :rake, 'cache:clear'
    end
  end

end



lock '3.2.1'
set :application, 'winter_cards'
set :repo_url, 'git@bitbucket.org:edgar_meinart/winter_cards.git'
set :deploy_to, "/home/web/apps/#{fetch(:application)}"

set :keep_releases, 3

set :scm, :git

set :rvm_ruby_string, :local

namespace :nginx do
  desc "Stop NGINX"
  task :stop do
    on roles(:web) do
      sudo '/etc/init.d/nginx stop'
    end
  end

  desc "Start NGINX"
  task :start do
    on roles(:web) do
      sudo '/etc/init.d/nginx start'
    end
  end

  desc "Reload NGINX"
  task :reload do
    on roles(:web) do
      sudo '/etc/init.d/nginx reload'
    end
  end
end

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join("#{fetch(:deploy_to)}/shared/restart.txt")
    end
  end
end
