WinterCards::Application.routes.draw do
  root to: 'home#index'

  post 'set_language', to: 'home#set_language'
  get 'ajax/add_to_basket', to: 'home#add_to_basket'
  get 'ajax/remove_from_basket', to: 'home#remove_from_basket'
  get 'ajax/basket_data', to: 'home#basket_data'
  get 'ajax/clear_basket', to: 'home#clear_basket'

  get '/cart',to: 'home#cart'

  get '/iwantthiscards' => 'deals#new'
  resources :deals, only: [:new , :create, :show], model: 'deal', path: 'checkout'


  get 'admin' => 'admin#index'

  namespace :admin do
    get 'deals', to: 'deals#index'
    delete 'destroy_deal'

    get 'show_deal'
    post 'checkout_deal'
    resources :cards, model: 'card', except: :show
  end

end
