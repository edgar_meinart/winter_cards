role :app, %w{web@168.63.69.32}
role :web, %w{web@168.63.69.32}
role :db,  %w{web@168.63.69.32}


set :rvm_ruby_version, 'ruby-2.1.1@gamewin'
set :rails_env, 'production'   

set :unicorn_config_path, "/home/web/apps/#{fetch(:application)}/current/config/unicorn.rb"
set :unicorn_pid, "/home/web/pids/unicorn-#{fetch(:application)}-#{fetch(:rails_env)}.pid"


server '168.63.69.32', user: 'web', roles: %w{web app}

after 'deploy:finished', 'unicorn:reload'